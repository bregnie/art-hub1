(function ($) {
   $(document).ready(function () {

        $('#m').change(function () {
            $.ajax({
                type: "POST",
               
                data: {
                    value: $("#m option:selected").val()
                 
                    
                },
                success: function (data) {
                    var div = $("#m option:selected").val();
                
                    $('#a').html(div);
                },
                error: function (data) {
                    alert("mo");
                }
            });

        });
   });
})(jQuery);
var date = new Date();
var data = [

];
function download_csv() {

    var date = new Date();
    var option = document.getElementById("a").innerHTML;
    console.log(option);
    var oTable = document.getElementById('dataTable');
    var name = document.getElementById("artistid").innerHTML;
    var title = name + '/' + date.getMonth() + '/' + date.getDate();
    //gets rows of table
    var rowLength = oTable.rows.length;

    //loops through rows    
    for (i = 1; i < rowLength; i++) {
        data1 = [];
        //gets cells of current row
        var oCells = oTable.rows.item(i).cells;

        //gets amount of cells of current row
        var cellLength = oCells.length;

        //loops through each cell in current row
        for (var j = 1; j < cellLength; j++) {
            /* get your cell info here */
            if (j == 6) {
              
                var cellVal = oCells.item(j).innerHTML;
                var mol = option * cellVal;
                data1.push(cellVal - mol);
            }else {

                var cellVal = oCells.item(j).innerHTML;
                data1.push(cellVal);
            }
        }
        data.push(data1);
    }
    var csv = 'SKU,Title,Media Description,Dimension,Quantity,Retial Price,FramePrice\n';
    data.forEach(function (row) {
        csv += row.join(',');
        csv += "\n";
    });

    console.log(csv);
    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = title +'/art.csv';
    hiddenElement.click();
    data = [];
   
}

function addRow(tableID) {

    var table = document.getElementById(tableID);

    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
    element1.type = "checkbox";
    element1.name = "chkbox[]";
    cell1.appendChild(element1);

    var cell2 = row.insertCell(1);
    cell2.innerHTML = "<input type='text' name='title[]'>";

    var cell3 = row.insertCell(2);
    cell3.innerHTML = "<input type='text'  name='cat[]' />";

    var cell4 = row.insertCell(3);
    cell4.innerHTML = "<input type='text'  name='dim[]' />";

    var cell5 = row.insertCell(4);
    cell5.innerHTML = "<input type='text' name='qty[]'>";

    var cell6 = row.insertCell(5);
    cell6.innerHTML = "<input type='text'  name='price[]' />";

    var cell7 = row.insertCell(6);
    cell7.innerHTML = "<input type='text'  name='sp[]' />";
}
function deleteRow(tableID) {
    try {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;

        for (var i = 0; i < rowCount; i++) {
            var row = table.rows[i];
            var chkbox = row.cells[0].childNodes[0];
            if (null != chkbox && true == chkbox.checked) {
                table.deleteRow(i);
                rowCount--;
                i--;
            }
        }
    } catch (e) {
        alert(e);
    }
}


function download_csv_artist() {
    var oTable = document.getElementById('artistInfo');
    var name = document.getElementById("artistid").innerHTML;
    var title = name + '/' + date.getMonth() + '/' + date.getDate();
    //gets rows of table
    var rowLength = oTable.rows.length;

    //loops through rows    
    for (i = 1; i < rowLength; i++) {
        data1 = [];
        //gets cells of current row
        var oCells = oTable.rows.item(i).cells;

        //gets amount of cells of current row
        var cellLength = oCells.length;

        //loops through each cell in current row
        for (var j = 1; j < cellLength; j++) {
            /* get your cell info here */
            var cellVal = oCells.item(j).innerHTML;
            data1.push(cellVal);

        }
        data.push(data1);
    }
    var csv = 'Name, MiddleIntial, Phone#, Website, Email, Address, City, State, Zip, ArtistCategory\n';
    data.forEach(function (row) {
        csv += row.join(',');
        csv += "\n";
    });

    console.log(csv);
    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = title +'/artist.csv';
    hiddenElement.click();
    data = [];

}
function download_csv_all_artist() {
    var oTable = document.getElementById('artist');
    var name = document.getElementById("artistid").innerHTML;
    var title = name + '/' + date.getMonth() + '/' + date.getDate();
    //gets rows of table
    var rowLength = oTable.rows.length;

    //loops through rows    
    for (i = 1; i < rowLength; i++) {
        data1 = [];
        //gets cells of current row
        var oCells = oTable.rows.item(i).cells;

        //gets amount of cells of current row
        var cellLength = oCells.length;

        //loops through each cell in current row
        for (var j = 1; j < cellLength; j++) {
            /* get your cell info here */
            var cellVal = oCells.item(j).innerHTML;
            data1.push(cellVal);

        }
        data.push(data1);
    }
    var csv = 'Name, MiddleIntial, Phone#, Website, Email, Address, City, State, Zip, ArtistCategory\n';
    data.forEach(function (row) {
        csv += row.join(',');
        csv += "\n";
    });

    console.log(csv);
    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = title +'/artist.csv';
    hiddenElement.click();
    data = [];

}
var priceData = [];
var priceData1 = [];

function priceCards() {
         var option = document.getElementById("a").innerHTML;
        var table = document.getElementById("dataTable");
        var rowCount = table.rows.length;
        var name = document.getElementById("name").innerHTML;
        var id = document.getElementById("artistid").innerHTML;
        var title = id + '/' + date.getMonth() + '/' + date.getDate();

        for (var i = 0; i < rowCount; i++) {
            
            var row = table.rows[i];
            var chkbox = row.cells[0].childNodes[0];
            if (null != chkbox && true == chkbox.checked) {
                var oCells = table.rows.item(i).cells;
                priceData1 = [];
                priceData1.push(name);

                for (x = 1; x < oCells.length; x++) {
                    if(x == 6){
                    var cellVal = oCells.item(x).innerHTML;
                    var mil = option * cellVal;
                    priceData1.push(cellVal - mil);
                    } else{
                        var cellVal = oCells.item(x).innerHTML;
                        priceData1.push(cellVal);
                    }
                }
                
                priceData.push(priceData1);
         
                
               
            }
          
        }
        console.log(priceData);
        var csv = 'Artist Name, SKU, Title, Media Desciption, Dimension, Quantity, Retail Price, Frame Price \n';
        priceData.forEach(function (row) {
            csv += row.join(',');
            csv += "\n";
        });

        console.log(csv);
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_blank';
        hiddenElement.download = title +  '/art.csv';
        hiddenElement.click();
        
  
    
}
function upload(){
  var formData = new FormData();
  formData.append("action", "upload-attachment");
	
  var fileInputElement = document.getElementById("file");
  formData.append("async-upload", fileInputElement.files[0]);
  formData.append("name", fileInputElement.files[0].name);
  	
  //also available on page from _wpPluploadSettings.defaults.multipart_params._wpnonce

  formData.append("_wpnonce", "<?php echo $my_nonce; ?>");
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange=function(){
    if (xhr.readyState==4 && xhr.status==200){
      console.log(xhr.responseText);
    }
  }
  xhr.open("POST","/wp-admin/async-upload.php",true);
  xhr.send(formData);
}


