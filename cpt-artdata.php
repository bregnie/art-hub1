﻿<?php
function register_art_data_content_type() {
	$post_labels = array(
		'name' 			    => 'ArtistData',
		'singular_name' 	=> 'ArtData',
		'add_new' 		    => 'Add New',
		'add_new_item'  	=> 'Add New Art Data',
		'edit'		        => 'Edit',
		'edit_item'	        => 'Edit User Data',
		'new_item'	        => 'New User Data',
		'view' 			    => 'View User Data',
		'view_item' 		=> 'View User Data',
		'search_term'   	=> 'Search User Data',
		'parent' 		    => 'Parent User Data',
		'not_found' 		=> 'No User Data found',
		'not_found_in_trash' 	=> 'No User Data in Trash'
	);
	register_post_type( 'artdata', array( 'labels' => $post_labels, 'public' => true, 'has_archive' => true  ) );
	$tax_labels = array(
		'name'              => _x( 'Art', 'text-domain' ),
		'singular_name'     => _x( 'Art', 'text-domain' ),
		'search_items'      => __( 'Search Art', 'text-domain' ),
		'all_items'         => __( 'All Art', 'text-domain' ),
		'parent_item'       => __( 'Parent Art', 'text-domain' ),
		'parent_item_colon' => __( 'Parent Art:', 'text-domain' ),
		'edit_item'         => __( 'Edit Art', 'text-domain' ),
		'update_item'       => __( 'Update Art', 'text-domain' ),
		'add_new_item'      => __( 'Add New Art', 'text-domain' ),
		'new_item_name'     => __( 'New Art Name', 'text-domain' ),
		'menu_name'         => __( 'Art', 'text-domain' ),

	);
	register_taxonomy( 'ArtWork', 'artdata', array( 'labels' => $tax_labels, 'rewrite' =>  array('slug' => 'location', 'with_front' => false) ) );
}
add_action( 'init', 'register_art_data_content_type' );
?>