﻿<?php get_header(); ?>


<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php echo do_shortcode("[wpc_client_files_list file_type='assigned' show_sort='yes' sort_type='date' sort='asc' categories_sort_type='name' categories_sort='asc' show_tags='yes' show_description='yes' show_date='yes' show_size='yes' show_author='yes' category='2' with_subcategories='no' show_file_cats='no' show_last_download_date='yes' show_thumbnails='yes' show_search='yes' show_filters='yes' filter_condition='or' show_pagination='yes' show_pagination_by='5' no_text='You don't have any files' /]");?>

     

                    <TABLE class="artInfo" width="600" border="1">

                        <colgroup>

                            <thead>

                                <tr>
                                    <colgroup>
                                        <col span="1" class="check">
                                        <col span="6" width="3000px" class="input">
                                    </colgroup>
                                    <th></th>
                                    <th>Title</th>
                                    <th>Media Description</th>
                                    <th>Dimensions</th>
                                    <th>Quantity</th>
                                    <th id="Retial Price">Retail Price</th>
                                    <th>Frame Price</th>
                                </tr>

                            </thead>
                            <tbody class="artInfo" id="dataTable">
                                <tr>
                                    <td><input type='checkbox' name='checkbox[]'></td>
                                    <td><input type='text' name='title[]'></td>"
                                    <td><input type='text' name='cat[]'></td>
                                    <td id="currencyinput"><input type='textarea' placeholder='$0.00' name='price[]'></td>
                                    <td><input type='text' name='qty[]'></td>
                                    <td><input type='text' name='dim[]'></td>
                                    <td><input type='text' name='sp[]'></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>

                                    <td><INPUT class="artInfoButton" type="button" value="Add" onClick="addRow('dataTable')" /></td>
                                    <td><INPUT class="artInfoButton" type="button" value="Delete Check" onClick="deleteRow('dataTable')" /></td>
                                </tr>
                            </tfoot>
                    </TABLE>

                    <input class="submit" name="export" type="submit" id="user-submitted-button" type="submit" value="submit">

            </form>

    </main>
</div>
<?php get_footer(); ?>	