<?php

   /*
   Plugin Name: art-hub
   description: 
  a plugin to create awesomeness and spread joy
   Version: 1.2
   Author: Elliott Bregni
   License: GPL2
   */

/**
*JavaScript 
*/
	

require_once dirname( __FILE__ ) .'/template.php';
require_once( 'cpt-artdata.php' );









add_action( 'wp_enqueue_scripts', 'table_script' ,'',time() );

function table_script(){
	$action = empty( $_REQUEST['action'] ) ? '' : $_REQUEST['action'];
	
	wp_enqueue_script( 'table-script', get_stylesheet_directory_uri() . '/js/table.js', array( 'jquery' ) );
		wp_enqueue_script('jsPDF', get_stylesheet_directory_uri() . '/js/jsPDF.min.js', array('jquery') );
		wp_register_script('jsPDF');
	wp_localize_script( 'table-script', 'sbg_ajax', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		 //'upload_url' => admin_url('async-upload.php'),
		 //'nonce'      => wp_create_nonce('media-form')


      )
  );
	if($_SERVER['REQUEST_METHOD'] === 'POST' && is_page_template('page-artistform.php') || is_page_template('page-ArtistTables.php')) {
		// Send the email...
		jal_install_data();
		tab_install_data();
		single_post_insert();
		
		
	}
	

}

/*
add_action( 'plugins_loaded', 'myplugin_update_db_check' );

global $jal_db_version;
$jal_db_version = '1.0';

function jal_install() {
	global $wpdb;
	global $jal_db_version;

	$table_name = $wpdb->prefix . 'artist';
	
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		fname VARCHAR(25) NOT NULL,
		lname VARCHAR(40) NOT NULL,
		mInitial VARCHAR(2) NOT NULL,
		phone VARCHAR(10) NOT NULL,
		email VARCHAR(25) NOT NULL,
		website tinytext NOT NULL,
		address tinytext NOT NULL,
		city tinytext NOT NULL,
		state tinytext NOT NULL,
		zip tinytext NOT NULL,
		statement text NOT NULL,
		res text NOT NULL,
		cat tinytext NOT NULL,
		user tinytext NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'jal_db_version', $jal_db_version );
}
*/
function jal_install_data() {
	global $wpdb;
	
	$fname = $_POST['fNAme'];
	$lname = $_POST['lname'];
	$i = $_POST['i'];
	$phone = $_POST['phone'];
	$website = $_POST['website'];
	$email = $_POST['email'];
	$address = $_POST['address'];
	$city = $_POST['city'];
	$state = $_POST['state'];
	$zip = $_POST['zip'];
	$statement = $_POST['user-entery-content'];
	$res = $_POST['user-entery-content2'];
	$artcat = $_POST['ArtistCategory'];

	$table_name = $wpdb->prefix . 'artist';
	$current_user = wp_get_current_user();
	$user = $current_user-> ID;
	$wpdb->insert( 
		$table_name, 
		array( 
			'time' => current_time( 'mysql' ), 
			'fname' => $fname, 
			'lname' => $lname,
			'mInitial' => $i,
			'phone' => $phone,
			'email' => $email,
			'website' => $website,
			'address' => $address,
			'city' => $city,
			'state' => $state,
			'zip' => $zip,
			'statement' => $statement,
			'res' => $res,
			'cat'=> $artcat,
			'user' => $user
		) 
	);
}
register_activation_hook( __FILE__, 'jal_install' );
register_activation_hook( __FILE__, 'jal_install_data' );

global $tab_db_version;
$tab_db_version = '1.0';
/*
function tab_install() {
	global $wpdb;
	global $tab_db_version;

	$table_name = $wpdb->prefix . 'art2';
	
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		title VARCHAR(25) NOT NULL,
		cat VARCHAR(25) NOT NULL,
		qty VARCHAR(25) NOT NULL,
		price DECIMAL NOT NULL,
		dim VARCHAR(25) NOT NULL,
		sp VARCHAR(25) NOT NULL,
	
		user tinytext NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'tab_db_version', $tab_db_version );
	
}
*/
function tab_install_data() {
    global $wpdb;


    $current_user = wp_get_current_user();
    $user = $current_user->ID;
    $table_name = $wpdb->prefix . 'art2';
    $rowCount = count($_POST['title']);


    // Note that this assumes all the variables will correctly be submitted as arrays of the same length. For completeness and robustness you could add a !empty check for each value.
    for ($i = 0; $i < $rowCount; $i++) {
	//artist
	


        //art
        $title = $_POST['title'][$i];
        $cat = $_POST['cat'][$i];
        $qty = $_POST['qty'][$i];
        $price = $_POST['price'][$i];
        $dim = $_POST['dim'][$i];
        $sp = $_POST['sp'][$i];

        $wpdb->insert(
            $table_name,
            array(
                'time' => current_time( 'mysql' ),
                'title' => $title,
                'cat' => $cat,
                'qty' => $qty,
                'price' => $price,
                'dim' => $dim,
                'sp' => $sp,
                'user' => $user
            )
			
        );
    }
}

register_activation_hook( __FILE__, 'tab_install_data' );
register_activation_hook( __FILE__, 'tab_install' );


function single_post_insert() {

	
	$fname = $_POST['fNAme'];
    $lname = $_POST['lname'];
    $i = $_POST['i'];
	$phone = $_POST['phone'];
	$website = $_POST['website'];
	$email = $_POST['email'];
	$address = $_POST['address'];
	$city = $_POST['city'];
	$state = $_POST['state'];
	$zip = $_POST['zip'];
	$statement = $_POST['user-entery-content'];
	$res = $_POST['user-entery-content2'];
	$artcat = $_POST['ArtistCategory'];

    $idF = substr(strip_tags($fname),0,1);
    $idL = substr(strip_tags($lname),0,1);
	$i = substr(strip_tags($i),0,1);
    $year =  date('y');
    $current_user = wp_get_current_user();
    $user = $current_user-> ID;
    $artistid = $idF. $i.$idL. $year;
    $new_post = array(
        'post_title'    =>"$artistid",
        'post_content'  => "",
        'post_status'   => 'publish',
        'post_type'     => 'artdata',
		'post_author'  => "$user",
		'meta_input' => array("user" => "$user", "First Name" => "$fname" ),
		
		

    );
    //insert the the post into database by passing $new_post to wp_insert_post
    //store our post ID in a variable $pid
    wp_insert_post($new_post);
	wp_redirect('http://localhost:82/wordpress/artisttable');
    //echo json_encode(array('flag'=>'1'));
    die();
}



/*
function foo() {



    $new_post = array(
        'post_title'    =>"w",
        'post_content'  => "e",
        'post_status'   => 'publish',
        'post_type'     => 'post',

		
		

    );
	
    //insert the the post into database by passing $new_post to wp_insert_post
    //store our post ID in a variable $pid
    wp_insert_post($new_post);
    //echo json_encode(array('flag'=>'1'));
    die();
}
add_action('wp_ajax_foo', 'foo' ); // executed when logged in
add_action('wp_ajax_nopriv_foo', 'foo' ); // executed when logged out
*/
?>
