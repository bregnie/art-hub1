﻿<?php get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	
	<div id="form">
		<form method="post" action="">
		
		<div class="top">
		<div class="formRow">
		<label class="hub" for="fName">First Name</label>
		<input required class="hub"name="fNAme" type="text" id="fName"  >

		<label class="hub1" for="lname">Last Name</label>
		<input required class="hub1" name="lname" type="text" id="lName"  >
	
		</div>
		<div class="formRow">
		<label class="hub" id="m" for="i">Middle Inital</label>
		<input required class="hub" name="i" type="text" id="mInitial" >
		<label class="hub1" for="phone">Phone</label>
		<input required align="left" class="hub1" name="phone" type="text" id="phone"  >
		</div>
		<div class="formRow">
		<label class="hub" for="website">Website</label>
		<input  class="hub" name="website" type="text" id="website">
		<label class="hub1" for="email">Email</label>
		<input align="left" class="hub1" name="email" type="text" id="email"  >
		</div>
		<div class="formRow">
		<label class="hub" id="address" for="address">Address</label>
		<input required class="hub" id="a" name="address" type="text" id="address"  >
		<label class="hub1" for="city">City</label>
		<input  required class="hub1" name="city" type="text" id="city"  >
		</div>
		<div class="formRow">
		
		<label class="hub" for="state">State</label>
		<input class="hub" name="state" type="text" id="state"  >
		<label class="hub1" for="zip">Zip</label>
		<input class="hub1" name="zip" type="text" id="zip"  >
	
		</div>
		<div class="formRow">
		<label  for="ArtistCategory">Artist Category</label>
		<select class="hub" name="ArtistCategory" id="ArtCat" " >
				<option value="Ceramics:" selected="selected">
						Ceramics:					</option>
										<option value="Childern's Book">
						Childern's Book					</option>
										<option value="Fibers">
						Fibers					</option>
										<option value="Glass">
						Glass					</option>
										<option value="Illustration">
						Illustration					</option>
										<option value="Jewelers">
						Jewelers					</option>
										<option value="Metal-Blacksmiths">
						Metal-Blacksmiths					</option>
										<option value="Mixed Media" >
						Mixed Media					</option>
										<option value="Painters-O-A-W-P">
						Painters-O-A-W-P					</option>
										<option value="Sculpture">
						Sculpture					</option>
										<option value="Woord">
						Wood					</option>
        </select>
	
		</div>
		
		<div class="textArea">
		<label id="AS" for="user-entery-content">Artist Statment</label>
        <textarea class="AS" name="user-entery-content" id="user-entery-content">  </textarea>
		</div>
		<div class="textArea">
		<label id="SP" for="user-entery-content2">Special Notes</label>
        <textarea id="SP" name="user-entery-content2" id="user-entery-content2">  </textarea>
		</div>





            <TABLE class="artInfo" width="600" border="1">

                <colgroup>

                <thead>

                <tr>
                    <colgroup>
                        <col span="1" class ="check">
                        <col span="6" width="3000px" class="input">
                    </colgroup>
                    <th></th>
                    <th>Title</th>
                    <th>Media Description</th>
                    <th>Dimensions</th>
                    <th>Quantity</th>
                    <th id="Retial Price" >Retail Price</th>
                    <th>Frame Price</th>
                </tr>

                </thead>
                <tbody class="artInfo" id="dataTable">
                <tr>
                    <td><input type='checkbox' name='checkbox[]'></td>
                    <td><input required  type='text' name='title[]'></td>"
                    <td><input required type='text' name='cat[]'></td>
                    <td><input required type='text' name='dim[]'></td>
                    <td><input required type='text' name='qty[]'></td>
                    <td><input required placeholder='$0.00'  type='text' name='price[]'></td>
					<td><input required placeholder='$0.00' type='text' name='sp[]'></td>
                </tr>
                </tbody>
            <tfoot>
            <tr>

                <td><INPUT class="artInfoButton" type="button" value="Add" onClick="addRow('dataTable')" /></td>
            <td><INPUT class="artInfoButton" type="button" value="Delete Check" onClick="deleteRow('dataTable')" /></td>
            </tr>
            </tfoot>
            </table>
		
		<input class="submit"  name="export" type="submit" id="download" type="submit" value="submit">
	
	</form>
	
	</main>
</div>
<?php get_footer(); ?>	